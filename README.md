# devtest

A minimal R package that passes `devtools::check` and which is used to [test `devtools`](https://github.com/hadley/devtools/issues/1157) Bitbucket functionality.